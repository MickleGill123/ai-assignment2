//============================================================================
// Name        : assignment2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
//fun
#include <iostream>
#include <map>
#include <iterator>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include <unordered_set>
#include <time.h>
#include <chrono>
using namespace std;
using namespace std::chrono;

// types of variable 1:Mij 2:Eij  3:eij 4:Xij 5:X'ij
//
struct variable
{
	
	
	int type;
	int i;
	int j;
	int k;
	int l;
	bool negation;

};

int m = 0;

int M = 0;

vector<vector<struct variable>> cnf;

int map_to_int (variable v){
	
	int res =0;
	
	if (v.type==1) res= (v.i-1)*M + v.j ;
	
	else if (v.type==2) res= (v.i-1)*M + v.j + M*m ;
	
	else if (v.type==3) res= (v.i-1)*M + v.j + M*m + M*M;
	
	else if (v.type==4) res= (v.i-1)*M*M*m + (v.j-1)*M*m + (v.k-1)*M + v.l +  M*m + M*M + m*m  ;
	
	else if (v.type==5) res= (v.i-1)*M*M*m + (v.j-1)*M*m + (v.k-1)*M + v.l +  M*m + M*M + m*m + M*M*m*m  ;
	
	if (v.negation) return res;
	else return (-1*res);
	
}

string clause_to_string (vector<variable>)
{
	string s = ""; variable v ; int val ;
	
	for(int i=0; i < vector.size(); i++){
   		v =	vector[i];
   		val = map_to_int(v);
   		if (s!="") s = s + " " + (string)val ;
   		else s = s+ (string)val;
	}
	
	s = s + " 0";
	 return s ;
	 
}

pair<int,int> seperator(string a){

	int length = a.size();

	int breakpoint = 0;

	pair<int,int> result;


	for(int i=0;i<length;i++){

		char d = a.at(i);

		if(d == 32){
			breakpoint =i;
			break;
		}

	}

	result.first = stoi(a.substr(0,breakpoint));

	result.second = stoi(a.substr(breakpoint+1,length-breakpoint-1));

	return result;
}

void readformn(){

	string line;

	ifstream myfileq ("/home/mickle/Desktop/1.txt");

	while (getline (myfileq,line)){

		if(line == "0 0"){
			break;
		}

		if(line.size() > 0){

			pair<int,int> ans = seperator(line);

			if(ans.first>M){
				M = ans.first;
			}

			if(ans.second>M){
				M = ans.second;
			}

		}


	}

	while (getline (myfileq,line)){

		if(line.size() > 0){

			pair<int,int> ans = seperator(line);

			if(ans.first>m){
				m = ans.first;
			}

			if(ans.second>M){
				m = ans.second;
			}

		}
	}

}

int get(int i,int j){
	return i+j*m;
}

void read(){

	bool eij[m*m];

	bool Eij[m*m];

	int* indegm = new int [m];

	int* outdegm = new int [m];

	int* indegM = new int [M];

	int* outdegM = new int [M];

	variable newone;

	vector<variable> toinsert;

	string line;

	ifstream myfileq ("/home/mickle/Desktop/1.txt");

	while (getline (myfileq,line)){

		if(line == "0 0"){
			break;
		}

		if(line.size() > 0){

			pair<int,int> ans = seperator(line);

			Eij[get((ans.first-1),(ans.second-1))] = true;

			indegM[ans.second-1] = indegM[ans.second-1]+1;

			outdegM[ans.first-1] = outdegM[ans.first-1]+1;

		}


	}

	while (getline (myfileq,line)){

		if(line.size() > 0){

			pair<int,int> ans = seperator(line);

		    eij[get(ans.first-1,ans.second-1)] = true;

			indegm[ans.second-1] = indegm[ans.second-1]+1;

			outdegm[ans.first-1] = outdegm[ans.first-1]+1;

		}

	}

	for(int i=1;i<=M;i++){

		for(int j=1;j<=M;j++){

			newone.type = 2;

			newone.i = i;

			newone.j = j;

			newone.negation = Eij[get((i-1),(j-1))];

			toinsert.push_back(newone);

			cnf.push_back(toinsert);

			toinsert.clear();

		}

	}

	for(int i=1;i<=m;i++){

		for(int j=1;j<=m;j++){


			newone.type = 3;

			newone.i = i;

			newone.j = j;

			newone.negation = eij[get(i-1,j-1)];

			toinsert.push_back(newone);

			cnf.push_back(toinsert);

			toinsert.clear();

		}

	}

	for(int i=1;i<=m;i++){

		for(int j=1;j<=M;j++){

			if(indegm[i-1]>indegM[j-1]){

				newone.type = 1;

				newone.i = i;

				newone.j = j;

				newone.negation = false;

				toinsert.push_back(newone);

				cnf.push_back(toinsert);

				toinsert.clear();

			}
			else{

				if(outdegm[i-1] != outdegM[j-1]){

					newone.type = 1;

					newone.i = i;

					newone.j = j;

					newone.negation = false;

					toinsert.push_back(newone);

					cnf.push_back(toinsert);

					toinsert.clear();

				}

			}

		}

	}



}

void firstclause(){


	variable newone;

	vector<variable> toinsert;


	for(int k=1;k<=m;k++){

		for(int l=1;l<=M;l++){

			newone.negation = true;
			newone.i = k;
			newone.j = l;
			newone.type = 1;

			toinsert.push_back(newone);


		}

		cnf.push_back(toinsert)

		toinsert.clear();

	}


}

void secondclause(){

	variable newone1;

	variable newone2;

	vector<variable> toinsert;


	for(int k=1;k<=m;k++){

		for(int l=1;l<=M-1;l++){

			for(int p=l+1;p<=M;p++){

				newone1.negation = false;
				newone1.i = k;
				newone1.j = l;
				newone1.type = 1;

				newone2.negation = false;
				newone2.i = k;
				newone2.j = p;
				newone2.type = 1;


				toinsert.push_back(newone1);

				toinsert.push_back(newone2);

				cnf.push_back(toinsert);

				toinsert.clear();

			}

		}


	}

}

void thirdclause(){

	variable newone1;

	variable newone2;

	vector<variable> toinsert;

	for(int k=1;k<=M;k++){

		for(int l=1;l<=m-1;l++){

			for(int p=l+1;p<=m;p++){

				newone1.negation = false;
				newone1.i = k;
				newone1.j = l;

				newone1.type = 1;
				newone2.negation = false;
				newone2.i = p;
				newone2.j = l;
				newone2.type = 1;

				toinsert.push_back(newone1);

				toinsert.push_back(newone2);

				cnf.push_back(toinsert);

				toinsert.clear();

			}

		}

	}

}

void fourthclause(){

	variable newone1;// -Mij

	variable newone2;// -eik
	
	variable newone3;// -Mkl
	
	variable newone4;// Ejl

	vector<variable> toinsert;

	for (int i=1 ; i<=m ; i++){
	
	for(int j=1;j<=M;j++){

		for(int k=1;k<=m;k++){

			for(int l=1;l<=M;l++){

				newone1.negation = false;
				newone1.i = i;
				newone1.j = j;
				newone1.type = 1;
				
				newone2.negation = false;
				newone2.i = i;
				newone2.j = k;
				newone2.type = 3;
				
				newone3.negation = false;
				newone3.i = k;
				newone3.j = l;
				newone3.type = 1;
				
				newone4.negation = true;
				newone4.i = j;
				newone4.j = l;
				newone4.type = 2;

				toinsert.push_back(newone1);

				toinsert.push_back(newone2);
				
				toinsert.push_back(newone3);
				
				toinsert.push_back(newone4);

				cnf.push_back(toinsert);

				toinsert.clear();

			}

		}

	} 
	}

}

void fifthclause(){

	variable newone1;// -Mij

	variable newone2;// eik
	
	variable newone3;// -Mkl
	
	variable newone4;// -Ejl

	vector<variable> toinsert;

	for (int i=1 ; i<=m ; i++){
	
	for(int j=1;j<=M;j++){

		for(int k=1;k<=m;k++){

			for(int l=1;l<=M;l++){

				newone1.negation = false;
				newone1.i = i;
				newone1.j = j;
				newone1.type = 1;
				
				newone2.negation = true;
				newone2.i = i;
				newone2.j = k;
				newone2.type = 3;
				
				newone3.negation = false;
				newone3.i = k;
				newone3.j = l;
				newone3.type = 1;
				
				newone4.negation = false;
				newone4.i = j;
				newone4.j = l;
				newone4.type = 2;

				toinsert.push_back(newone1);

				toinsert.push_back(newone2);
				
				toinsert.push_back(newone3);
				
				toinsert.push_back(newone4);

				cnf.push_back(toinsert);

				toinsert.clear();

			}

		}

	} 
	}

}

void clause_for_Xijkl(){

	variable newone1;

	variable newone2;
	
	variable newone3;
	
	variable newone4;

	vector<variable> toinsert;


	for (int i=1 ; i<=m ; i++){
	
	for(int j=1;j<=M;j++){

		for(int k=1;k<=m;k++){

			for(int l=1;l<=M;l++){

			// -Xijkl V eik
			
				newone1.negation = false;
				newone1.i = i;
				newone1.j = j;
				newone1.k = k;
				newone1.l = l;
				newone1.type = 4;
				
				newone2.negation = true;
				newone2.i = i;
				newone2.j = k;
				newone2.type = 3;
				
				toinsert.push_back(newone1);
				toinsert.push_back(newone2);
				
				cnf.push_back(toinsert);
				toinsert.clear();
				
			// -Xijkl V Mkl
				
				newone1.negation = false;
				newone1.i = i;
				newone1.j = j;
				newone1.k = k;
				newone1.l = l;
				newone1.type = 4;
				
				newone2.negation = true;
				newone2.i = k;
				newone2.j = l;
				newone2.type = 1;
				
				toinsert.push_back(newone1);
				toinsert.push_back(newone2);
				
				cnf.push_back(toinsert);
				toinsert.clear();
				
			//  -Xijkl V -Ejl
			
				newone1.negation = false;
				newone1.i = i;
				newone1.j = j;
				newone1.k = k;
				newone1.l = l;
				newone1.type = 4;
				
				newone2.negation = false;
				newone2.i = j;
				newone2.j = l;
				newone2.type = 2;
				
				toinsert.push_back(newone1);
				toinsert.push_back(newone2);
				
				cnf.push_back(toinsert);
				toinsert.clear();
				
			// -eik V -Mkl V Ejl V Xijkl
			
				newone4.negation = false;
				newone4.i = i;
				newone4.j = k;
				newone4.type = 3;
				
				newone3.negation = false;
				newone3.i = k;
				newone3.j = l;
				newone3.type = 1;
				
				newone2.negation = true;
				newone2.i = j;
				newone2.j = l;
				newone2.type = 2;
				
				newone1.negation = true;
				newone1.i = i;
				newone1.j = j;
				newone1.k = k;
				newone1.l = l;
				newone1.type = 4;	
				
				toinsert.push_back(newone1);
				toinsert.push_back(newone2);
				toinsert.push_back(newone3);
				toinsert.push_back(newone4);
				
				cnf.push_back(toinsert);
				toinsert.clear();
				
			

			}

		}

	} 
	}

}


void clause_for_type5(){

	variable newone1;

	variable newone2;
	
	variable newone3;
	
	variable newone4;

	vector<variable> toinsert;


	for (int i=1 ; i<=m ; i++){
	
	for(int j=1;j<=M;j++){

		for(int k=1;k<=m;k++){

			for(int l=1;l<=M;l++){

			// -X'ijkl V -eik
			
				newone1.negation = false;
				newone1.i = i;
				newone1.j = j;
				newone1.k = k;
				newone1.l = l;
				newone1.type = 5;
				
				newone2.negation = false;
				newone2.i = i;
				newone2.j = k;
				newone2.type = 3;
				
				toinsert.push_back(newone1);
				toinsert.push_back(newone2);
				
				cnf.push_back(toinsert);
				toinsert.clear();
				
			// -X'ijkl V Mkl
				
				newone1.negation = false;
				newone1.i = i;
				newone1.j = j;
				newone1.k = k;
				newone1.l = l;
				newone1.type = 5;
				
				newone2.negation = true;
				newone2.i = k;
				newone2.j = l;
				newone2.type = 1;
				
				toinsert.push_back(newone1);
				toinsert.push_back(newone2);
				
				cnf.push_back(toinsert);
				toinsert.clear();
				
			//  -Xijkl V Ejl
			
				newone1.negation = false;
				newone1.i = i;
				newone1.j = j;
				newone1.k = k;
				newone1.l = l;
				newone1.type = 5;
				
				newone2.negation = true;
				newone2.i = j;
				newone2.j = l;
				newone2.type = 2;
				
				toinsert.push_back(newone1);
				toinsert.push_back(newone2);
				
				cnf.push_back(toinsert);
				toinsert.clear();
				
			// eik V -Mkl V -Ejl V X'ijkl
			
				newone4.negation = true;
				newone4.i = i;
				newone4.j = k;
				newone4.type = 3;
				
				newone3.negation = false;
				newone3.i = k;
				newone3.j = l;
				newone3.type = 1;
				
				newone2.negation = false;
				newone2.i = j;
				newone2.j = l;
				newone2.type = 2;
				
				newone1.negation = true;
				newone1.i = i;
				newone1.j = j;
				newone1.k = k;
				newone1.l = l;
				newone1.type = 5;	
				
				toinsert.push_back(newone1);
				toinsert.push_back(newone2);
				toinsert.push_back(newone3);
				toinsert.push_back(newone4);
				
				cnf.push_back(toinsert);
				toinsert.clear();
				
			

			}

		}

	} 
	}

}

void sixthclause(){

	variable newone1;

	variable newone2;
	
	variable newone3;
	
	variable newone4;

	vector<variable> toinsert;


	for (int i=1 ; i<=m ; i++){
	
	for(int j=1;j<=M;j++){
		
		newone1.negation = true;
		newone1.i = i;
		newone1.j = j;
		newone1.type = 1;
		
		toinsert.push_back(newone1);

		for(int k=1;k<=m;k++){

			for(int l=1;l<=M;l++){

				newone1.negation = true;
				newone1.i = i;
				newone1.j = j;
				newone1.k = k;
				newone1.l = l;
				newone1.type = 5;
				
				newone2.negation = true;
				newone2.i = i;
				newone2.j = j;
				newone2.k = k;
				newone2.l = l;
				newone2.type = 4;
				
				toinsert.push_back(newone1);	
				toinsert.push_back(newone2);	
			

			}

		}
		
	cnf.push_back(toinsert);
	toinsert.clear();	

	} 
	}

}


int main() {

	read();
	
	readformn();

	firstclause();

	secondclause();

	thirdclause();

	//cout << cnf.at(6).at(0).i << endl;
	
	for (int i=0 ;i< cnf.size();i++){
		string s = clause_to_string(cnf[i]);
		
		// print s to the file
	
	}

	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	return 0;
}
